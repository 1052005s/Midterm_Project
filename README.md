# Software Studio 2018 Spring Midterm Project

## Topic
* Forum
* Key functions
    1. Post list page
    2. Post page
    3. User page
    4. Post new topic and comment (Need Login) 

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description
<img src="md_img/postlist.PNG" width="576px" height="369px" alt="postlist"></img>
<h3> 網站介紹 </h3>
此網站為會員制論壇，提供使用者在此發布文章及留言。

1. <b>會員註冊</b>
    * 已註冊的會員可發布新文章及在所有文章中留言
    * 未註冊的使用者可以觀看他人文章及留言，但無法發布新文章及留言
    * 支援以Google帳戶作為會員登入

2. <b>文章列表</b>
    * 發布的文章會列在文章列表中，使用者在此頁面可以查看文章標題及發佈者

3. <b>文章頁面</b>
    * 點擊文章列表中的文章將會被導引至該文章的詳細頁面，使用者可在此頁面查看文章內容，並可在文章底部發布留言 (需會員登入尚可留言)  
    * 使用者可點擊文章頁面中的發布者暱稱(Author)查看發文者的暱稱及Email

4. <b>使用者頁面</b>
    * 在文章列表中點擊自己的會員名稱可進入使用者資料頁面，此頁面會顯示使用者的暱稱及Email
    
5. <b>發布文章</b>
    * (需註冊會員)文章列表中點擊New Topic後將導引至文章發布頁面，文章發布需要填寫文章主題(Subject)及文章內容(Content)
    * 文章內容支援輸入HTML/CSS語法讓使用者自定義文字格式

## Security Report (Optional)

## Student ID , Name and Template URL
-  學號: 1052005s
-  姓名: 林浩平
-  作品網址: https://midterm-c45f0.firebaseapp.com/
-  報告網址: https://gitlab.com/1052005s/Midterm_Project/blob/master/README.md

