var txtSignupUserName = document.getElementById("signupUserName");
var btnGoogle = document.getElementById("btnGoogle");

btnGoogle.addEventListener("click", function () {
    var userName = txtSignupUserName.value;
    // check current user status
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            
            // Save personal profile into specific firebase
            firebase.database().ref("userAccount/" + user.uid).set({
                // Member's profile and post 
                UserName: userName,
                UserEmail: "Google Sign-in User"
                //            avatar: www.xxx.jpeg
            }).then(function () {
                window.location = "index.html";
            }).catch(function (err) { console.log(err.message); alert(err.message) });
        }
    });
});