var btnPostComment = document.getElementById("btnPostComment");
        var myPostComment = document.getElementById("myPostComment");

        var postNow;
        var indexNum = 0;
        var count = 0;
        var first_count = 0;
        var second_count = 0;
        var snapData = firebase.database().ref("userPost");
        snapData.once("value").then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {

                if (indexNum == count) {
                    postNow = childSnapshot.key;
                    document.getElementById("author").innerHTML = childSnapshot.val().author;
                    document.getElementById("p_topic").innerHTML = childSnapshot.val().content;
                    document.getElementById("p_content").innerHTML = childSnapshot.val().content2;
                }
                count++;
            });
        }).then(function () {
            // read
            var searchData = firebase.database().ref("userPost/" + postNow + "/comment");

            searchData.once("value").then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    var postList = document.createElement("DIV");
                    // Create Post Page Element (link)
                    var postPara = document.createElement("H3");
                    var postPara2 = document.createElement("H3");
                    var br = document.createElement("BR");

                    postPara.setAttribute("style", "color:yellow; display:inline");
                    //////////////////////////////////// modi
                    var t2 = document.createTextNode(childSnapshot.val().author + ": ");
                    var t = document.createTextNode(childSnapshot.val().comment);
                    postPara2.appendChild(t2);
                    postPara.appendChild(t);
                    postPara.appendChild(br);
                    postList.appendChild(postPara2).appendChild(postPara);
                    first_count += 1;
                    //  document.getElementById("content").appendChild(postConn).appendChild(post);
                    document.getElementById("commentArea").appendChild(postList);
                });

                //add listener
                searchData.on('child_added', function (data) {
                    second_count += 1;
                    if (second_count > first_count) {
                        var postList = document.createElement("DIV");
                        // Create Post Page Element (link)
                        var postPara = document.createElement("H3");
                        var postPara2 = document.createElement("H3");
                        var br = document.createElement("BR");

                        postPara.setAttribute("style", "color:yellow; display:inline");
                        //////////////////////////////////// modi
                        var t2 = document.createTextNode(data.val().author + ": ");
                        var t = document.createTextNode(data.val().comment);
                        postPara2.appendChild(t2);
                        postPara.appendChild(t);
                        postPara.appendChild(br);
                        postList.appendChild(postPara2).appendChild(postPara);
                        first_count += 1;
                        //  document.getElementById("content").appendChild(postConn).appendChild(post);
                        document.getElementById("commentArea").appendChild(postList);
                    }
                    document.getElementById("myPostComment").value = "";
                });
            });
        });

        // Post button action
        btnPostComment.addEventListener("click", function () {
            var currentUser = firebase.auth().currentUser;

            // Read user profile from database
            if (currentUser != null) {
                var userData = firebase.database().ref("userAccount/" + currentUser.uid);
                var UserName;
                userData.on("value", function (snapshot) {
                    UserName = snapshot.val().UserName;
                    var newPostRef = firebase.database().ref("userPost/" + postNow + "/comment").push();
                    newPostRef.set({
                        // Member's profile and post 
                        author: UserName,
                        comment: myPostComment.value,
                        //            avatar: www.xxx.jpeg
                    }).catch(function (err) {
                        console.log(err.message);
                        alert(err.message)
                    });
                    //         txtMyPost.value = "";
                });
            } else
                alert("you have to sign in to post");

        });