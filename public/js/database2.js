var btnPost = document.getElementById("btnPost");
var txtMyPost = document.getElementById("myPost");
var txtMyPost2 = document.getElementById("myPostC");

var postIndex = [];

// Post button action
btnPost.addEventListener("click", function () {
    txtMyPost2.value = txtMyPost2.value.replace(/\r?\n/g, '<br />');
    var currentUser = firebase.auth().currentUser;

    // Read user profile from database
    if (currentUser != null) {
        var userData = firebase.database().ref("userAccount/" + currentUser.uid);
        var UserName;
        userData.once("value").then(function (snapshot) {
            UserName = snapshot.val().UserName;

            var newPostRef = firebase.database().ref("userPost").push();
            postIndex.push(newPostRef.key);

            newPostRef.set({
                // Member's profile and post 
                author: UserName,
                uid: currentUser.uid,
                content: txtMyPost.value,
                content2: txtMyPost2.value
                //            avatar: www.xxx.jpeg
            }).then(function () {
                window.location = "index.html";
            }).catch(function (err) {
                console.log(err.message);
                alert(err.message)
            });
        });
    } else {
        alert("you have to sign in to post");
        window.location = "index.html";
    }
});