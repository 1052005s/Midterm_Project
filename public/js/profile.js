// Check current user status
firebase.auth().onAuthStateChanged(function (user) {
    firebase.database().ref("userAccount/" + user.uid).once("value").then(function (snapshot) {
        var whoAmI = document.getElementById("whoAmI");
        var myDes = document.getElementById("myDes");
        //    var imgPos = document.getElementById("imgPos");
        whoAmI.innerHTML = snapshot.val().UserName;
        myDes.innerHTML = "Email: " + snapshot.val().UserEmail;
        //    imgPos.setAttribute("src", snapshot.val().avatar);
    });
});

// Create a storage reference from our storage service
var storageRef = firebase.storage().ref();

// Upload
var selectedFile;
var btnUL = document.getElementById("btnPost");

btnUL.addEventListener("click", function () {
    var fileRef = storageRef.child("img/");
    fileRef.put(selectedFile).then(function (snapshot) {
        console.log("Uploaded a blob or file!");
    }).catch(function (error) {
        console.log(error.message);
    });
});