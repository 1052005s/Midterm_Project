var btnLogin = document.getElementById("btnLogin");
var btnLogout = document.getElementById("btnLogout");
var googleLogin = document.getElementById("googleLogin");
var txtUsername = document.getElementById("username")
var txtPassword = document.getElementById("password");


// Login button action
btnLogin.addEventListener("click", function () {
    var username = txtUsername.value;
    var password = txtPassword.value;
    firebase.auth().signInWithEmailAndPassword(username, password).then(function () {
        location.reload();
    }).catch(function (err) {
        console.log(err.message);
        alert(err);
    });
});

// Google Login button action
var provider = new firebase.auth.GoogleAuthProvider();
googleLogin.addEventListener("click", function () {
    firebase.auth().signInWithPopup(provider).then(function () {
        var currentUser = firebase.auth().currentUser;
        var googleNameCheck = firebase.database().ref("userAccount/" + currentUser.uid);
        googleNameCheck.once("value").then(function (check) {
            if (check.val() == null)
                window.location = "googleuserName.html";
            else
            window.location = "index.html";
        });
    }).catch(function (err) {
        console.log(err.message)
    });
});

// Try to make LogOut function so can be used in everywhere that need LogOug action
function LogOut() {
    firebase.auth().signOut();
}

// Check current user status
firebase.auth().onAuthStateChanged(function (user) {
    var loginInputDiv = document.getElementById("loginInputDiv");
    var loginStatusDiv = document.getElementById("loginStatusDiv");
    var loginStatusEmail = document.getElementById("loginStatusName");
    if (user) {
        loginInputDiv.style.display = "none";
        loginStatusDiv.style.display = "block";

        // Read user profile from database
        var userData = firebase.database().ref("userAccount/" + user.uid);
        userData.on("value", function (snapshot) {
            loginStatusEmail.innerHTML = snapshot.val().UserName;
        });
    } else {
        loginInputDiv.style.display = "block";
        loginStatusDiv.style.display = "none";
    }
});