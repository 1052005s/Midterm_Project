var btnPost = document.getElementById("btnPost");
var txtMyPost = document.getElementById("myPost");
var postNewCheck = document.getElementById("postNewCheck");


postNewCheck.addEventListener("click", function () {
    var currentUser = firebase.auth().currentUser;

    // Read user profile from database
    if (currentUser != null)
        window.location = "postnew.html";
    else
        alert("you have to sign in to post");
});

// Read member history posts 
var pageCode = [];
var pageCount = 0;

var first_count = 0;
var second_count = 0;
var snapData = firebase.database().ref("userPost");
snapData.once("value").then(function (snapshot) {
    snapshot.forEach(function (childSnapshot) {
        var postList = document.createElement("DIV");
        // Create Post Page Element (link)
        var postPara = document.createElement("H3");
        var postParaAuthor = document.createElement("H4");
        var postLink = document.createElement("A");
        var br = document.createElement("BR");
        pageCode.push(childSnapshot.key);
        postLink.setAttribute("href", "temp/" + pageCount + ".html");
        postParaAuthor.setAttribute("style", "color:grey");
        pageCount++;

        var t = document.createTextNode(childSnapshot.val().content);
        var r = document.createTextNode("by: " + childSnapshot.val().author);
        postPara.appendChild(t);
        postPara.appendChild(br);
        postParaAuthor.appendChild(r);


        postLink.appendChild(postPara);
        postLink.appendChild(postParaAuthor);
        postList.appendChild(postLink);

        first_count += 1;
        document.getElementById("content").appendChild(postList);
    });
    pageCount = 0;

    // Post new comment in database
    snapData.on("child_added", function (newdata) {
        second_count += 1;
        if (second_count > first_count) {
            var postList = document.createElement("DIV");
            // Create Post Page Element (link)
            var postPara = document.createElement("H3");
            var postLink = document.createElement("A");
            postLink.setAttribute("href", "postPage.html");
            var t = document.createTextNode(newdata.val().author + " say: " + newdata.val().content);

            postPara.appendChild(t);
            postLink.appendChild(postPara);
            postList.appendChild(postLink);
            document.getElementById("content").appendChild(postList);
        }
    });
}).catch(function (err) {
    console.log(err.message); /*alert(err.message)*/
});