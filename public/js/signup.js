var txtSignupUserName = document.getElementById("signupUserName");
var txtSignupEmail = document.getElementById("signupEmail");
var txtSignupPassword = document.getElementById("signupPassword")
var btnSignup = document.getElementById("btnSignup");

btnSignup.addEventListener("click", function () {
    var userName = txtSignupUserName.value;
    var userEmail = txtSignupEmail.value;
    var password = txtSignupPassword.value;
    firebase.auth().createUserWithEmailAndPassword(userEmail, password).catch(function (err) { console.log(err.message); alert(err.message) });

    // check current user status
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            // Save personal profile into specific firebase
            firebase.database().ref("userAccount/" + user.uid).set({
                // Member's profile and post 
                UserName: userName,
                UserEmail: userEmail,
                avatar: "img/avatar_default.png"
                //            avatar: www.xxx.jpeg
            }).then(function () {
           //     alert("Account create success");
                window.location = "index.html";
            }).catch(function (err) { console.log(err.message); alert(err.message) });
        }
    });
});
